public class Main {

    public static void main(String[] args) {
        // TP1
        // testFonctionVecteur2D();
        // testTourVauban();
        // testQuadrilatere();

        // TP2
        // testVecteurND();
        // testColiVecteur();

        // TP3
        // testMatrice();
        // testMultMatrice();

        // TP4
        // test3D();
        // test3DRotation();

        // TP5
        testNormaliseHomo();
    }

    public static void testFonctionVecteur2D() {
        // Plan p = new Plan();

        VecteurEn2D v1 = new VecteurEn2D(3, 4);
        VecteurEn2D v2 = new VecteurEn2D(4, 5);
        // p.dessinerPointEn2d(v1);

        System.out.println(v1.norme());

        v1.multiplicationScalaire(3);
        v1.displayVecteur();

        v1 = new VecteurEn2D(3, 4);
        v1.sommeVectorielle(v2);
        v1.displayVecteur();

        v1 = new VecteurEn2D(3, 4);
        System.out.println(v1.produitScalaire(v2));

        System.out.println(v1.estOrthogonal(v2));

        System.out.println(v1.estOrthogonal(new VecteurEn2D(0, 0)));

        v1.obtenirVectOthogonal().displayVecteur();

        VecteurEn2D v = new VecteurEn2D(0.1, 9);
        VecteurEn2D w = new VecteurEn2D(-4.5, 0.05);
        System.out.println(v.estOrthogonal(w));

        v.multiplicationScalaire(0.1);
        System.out.println(v.estOrthogonal(w));

        VecteurEn2D v3 = new VecteurEn2D(3, 4);
        VecteurEn2D v4 = new VecteurEn2D(12, 16);

        System.out.println(v3.estColineaire(v3));
        System.out.println(v3.estColineaire(v4));
    }

    public static void testTourVauban() {
        VecteurEn2D vA = new VecteurEn2D(-3, 2);
        VecteurEn2D vB = new VecteurEn2D(-2, 2);
        VecteurEn2D vC = new VecteurEn2D(-1.5, 2.87);
        VecteurEn2D vD = new VecteurEn2D(-2, 3.74);

        VecteurEn2D vAB = new VecteurEn2D(vB);
        vAB.sommeVectorielle(vA.inverseVecteur());
        VecteurEn2D vU = vAB.obtenirVectOthogonal();
        vU.multiplicationScalaire(1 / vU.norme());

        VecteurEn2D vBC = new VecteurEn2D(vC);
        vBC.sommeVectorielle(vB.inverseVecteur());
        VecteurEn2D vV = vBC.obtenirVectOthogonal();
        vV.multiplicationScalaire(1 / vV.norme());

        VecteurEn2D vCD = new VecteurEn2D(vD);
        vCD.sommeVectorielle(vC.inverseVecteur());
        VecteurEn2D vW = vCD.obtenirVectOthogonal();
        vW.multiplicationScalaire(1 / vW.norme());

    }

    public static void testQuadrilatere() {
        VecteurEn2D vA = new VecteurEn2D(-3, -2);
        VecteurEn2D vB = new VecteurEn2D(-1, 2);
        VecteurEn2D vC = new VecteurEn2D(1, 3);
        VecteurEn2D vD = new VecteurEn2D(1, 0);
        VecteurEn2D vE = new VecteurEn2D(-1, 1);
        VecteurEn2D vF = new VecteurEn2D(4, 1);
        VecteurEn2D vG = new VecteurEn2D(3, 4);
        VecteurEn2D vH = new VecteurEn2D(0, 3);

        VecteurEn2D vAB = vA.createNewVecteur(vB);
        VecteurEn2D vBC = vB.createNewVecteur(vC);
        VecteurEn2D vCD = vC.createNewVecteur(vD);
        VecteurEn2D vCE = vC.createNewVecteur(vE);
        VecteurEn2D vDF = vD.createNewVecteur(vF);
        VecteurEn2D vFG = vF.createNewVecteur(vG);
        VecteurEn2D vGH = vG.createNewVecteur(vH);

        testQuadrilatereForm(vAB, vBC, vCD);
        testQuadrilatereForm(vAB, vBC, vCE);
        testQuadrilatereForm(vDF, vFG, vGH);
    }

    public static void testQuadrilatereForm(final VecteurEn2D pV1, final VecteurEn2D pV2, final VecteurEn2D pV3) {
        if (pV1.estOrthogonal(pV2)) {
            if (pV1.norme() == pV2.norme()) {
                System.out.println("Carré");
            } else {
                System.out.println("Parallelogramme");
            }

        } else if (pV1.estColineaire(pV3)) {
            System.out.println("Trapèze");
        } else {
            System.out.println("Je ne peux rien dire ");
        }

    }

    public static void testVecteurND() {
        double[] tab1 = { 0, 1.5, 2 };
        Vecteur v1 = new Vecteur(tab1);
        double[] tab2 = { 1, 2, 3, 1 };
        Vecteur v2 = new Vecteur(tab2);

        Plan plan = new Plan();

        v1.displayVecteurInPlan(plan);
        v2.displayVecteurInPlan(plan);
    }

    public static void testColiVecteur() {

        double[] tab1 = { 1, 1 };
        Vecteur v1 = new Vecteur(tab1);
        double[] tab2 = { -4, -4 };
        Vecteur v2 = new Vecteur(tab2);

        System.out.println(v1.estColineaire(v2));
    }

    public static void testMatrice() {
        Matrice m1 = new Matrice(3, 3);
        System.out.println("" + m1.getNbLine() + " / " + m1.getNbCol());
        Matrice m2 = new Matrice(3, 4);
        System.out.println("" + m2.getNbLine() + " / " + m2.getNbCol());

        System.out.println("m1[2,2] = " + m1.getCoefficient(2, 2));
        System.out.println("m2[2,2] = " + m2.getCoefficient(2, 2));

        m1.display();
        m2.display();
    }

    public static void testMultMatrice() {
        double[][] tab1 = { { 0.0, -1.0, 2.0 }, { 1.0, 0.0, 1.0 } };
        Matrice m1 = new Matrice(tab1);
        m1.display();

        double[][] tab2 = { { 1.0 }, { 2.0 }, { 1.0 } };
        Matrice m2 = new Matrice(tab2);
        m2.display();

        Matrice m3 = m1.produitMatriciel(m2);
        m3.display();

        Matrice m4 = new Matrice(3, 3);
        m4.display();
        m4.setIdentite();
        m4.display();

        Matrice m5 = m1.produitMatriciel(m4);
        m5.display();

        m5.hometethie(2);
        m5.display();

        double[][] tab3 = { { 1.0, 2.0 }, { 2.0, 4.0 } };
        Matrice m6 = new Matrice(tab3);

        m6.display();
        m6.reflexion2D();
        m6.display();

        double[][] tab4 = { { 1.0, 2.0, 3.0 }, { 2.0, 4.0, 3.0 }, { 6.0, 1.0, 9.0 } };
        Matrice m7 = new Matrice(tab4);

        m7.display();
        m7.reflexion2D();
        m7.display();
        m7.reflexion3D();
        m7.display();
    }

    public static void test3D() {
        TestCube.test();
    }

    public static void test3DRotation() {
        TestCube.testRotation();
    }

    public static void testNormaliseHomo() {
        double[] tab = { 1.0, 2.0, 3.0, 4.0 };
        Vecteur v = new Vecteur(tab);
        v.normaliseHomogene();
        v.display();

        Matrice m = new Matrice(4, 4);
        m.setHomotethieHomogene3D(5);
        m.display();

        double[] tab2 = { 1.0, 2.0, 3.0, 1.0 };
        Vecteur v2 = new Vecteur(tab2);
        v2 = m.produitMatriciel(v2);
        v2.display();

        System.out.println("");

        double[][] tab3 = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 16 } };
        Matrice m2 = new Matrice(tab3);
        m2.display();
        m2.setProjectionOrthoOxyHomogene3d();
        m2.display();

    }
}

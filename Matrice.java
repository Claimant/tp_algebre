public class Matrice {

    private int aLine;
    private int aCol;

    private double[][] aMatrice;

    Matrice(final int pLine, final int pCol) {
        this.aMatrice = new double[pCol][pLine];
        this.aLine = pLine;
        this.aCol = pCol;
    }

    Matrice(final double[] pLine) {
        this(pLine.length, 1);
        this.aMatrice[0] = pLine;
    }

    Matrice(final double[][] pMatrice) {
        this(pMatrice.length, pMatrice[0].length);
        this.aMatrice = pMatrice;
    }

    public Vecteur getVecteur() {
        Vecteur v = new Vecteur(this.aMatrice[0]);
        return v;
    }

    public Matrice produitMatriciel(Matrice pM) {
        // System.out.println("" + this.getNbCol() + "/" + pM.getNbLine());
        if (this.getNbCol() == pM.getNbLine()) {
            int nb = this.getNbCol();
            Matrice produit = new Matrice(pM.getNbCol(), this.getNbLine());
            for (int i = 0; i < produit.aCol; i++) {
                for (int j = 0; j < produit.aLine; j++) {
                    double sum = 0.0;
                    for (int x = 0; x < nb; x++) {
                        // System.out.println("m1 : " + i + "/" + x + " => " + this.aMatrice[i][x]);
                        // System.out.println("m2 : " + x + "/" + j + " => " + pM.aMatrice[x][j]);
                        sum += this.aMatrice[i][x] * pM.aMatrice[x][j];
                    }
                    produit.aMatrice[i][j] = sum;
                }
            }
            return produit;
        }
        System.out.println("Pas les bonnes dimensions ");
        return null;
    }

    public Vecteur produitMatriciel(Vecteur pV) {
        if (this.aCol != pV.getDimension()) {
            System.out.println("Pas les memes dimensions");
            return null;
        }
        int x = pV.getDimension();
        Vecteur vReturned = new Vecteur(x);
        for (int i = 0; i < x; i++) {
            double value = 0;
            for (int j = 0; j < x; j++) {
                // System.out.println("m1 : " + i + "/" + j + " => " + this.aMatrice[i][j]);
                value += (pV.getCoordonnees(j) * this.getCoefficient(i, j));
            }
            vReturned.setCoordonnee(i, value);
        }
        return vReturned;
    }

    public void setIdentite() {
        if (aCol != aLine) {
            System.out.println("Impossible, la matrice n'est pas carrée");
            return;
        }
        for (int i = 0; i < aCol; i++) {
            for (int j = 0; j < aLine; j++) {
                if (i == j) {
                    aMatrice[i][j] = 1.0;
                } else {
                    aMatrice[i][j] = 0.0;
                }
            }
        }
    }

    public void hometethie(final double k) {
        this.setIdentite();
        for (int i = 0; i < aCol; i++) {
            for (int j = 0; j < aLine; j++) {
                if (i == j) {
                    aMatrice[i][j] = aMatrice[i][j] * k;
                }
            }
        }
    }

    public void symetrieCentrale() {
        hometethie(-1.0);
    }

    public void reflexion2D() {
        if (aCol != aLine || aCol != 2) {
            System.out.println("Impossible, la matrice n'est pas carrée ou de bonne dimension");
            return;
        }
        double[][] tab = { { 1.0, 0.0 }, { 0.0, -1.0 } };
        Matrice reflexion = new Matrice(tab);
        this.aMatrice = this.produitMatriciel(reflexion).aMatrice;
    }

    public void reflexion3D() {
        if (aCol != aLine || aCol != 3) {
            System.out.println("Impossible, la matrice n'est pas carrée ou de bonne dimension");
            return;
        }
        double[][] tab = { { 1.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 }, { 0.0, 0.0, -1.0 } };
        Matrice reflexion = new Matrice(tab);
        this.aMatrice = reflexion.aMatrice;
    }

    public void rotation2D(double A) {
        if (aCol != aLine || aCol != 2) {
            System.out.println("Impossible, la matrice n'est pas carrée ou de bonne dimension");
            return;
        }
        double[][] tab = { { Math.cos(Math.PI * A), -Math.sin(Math.PI * A) },
                { Math.sin(Math.PI * A), Math.cos(Math.PI * A) } };
        Matrice reflexion = new Matrice(tab);
        this.aMatrice = reflexion.aMatrice;
    }

    public void rotation3DX(double A) {
        if (aCol != aLine || aCol != 3) {
            System.out.println("Impossible, la matrice n'est pas carrée ou de bonne dimension");
            return;
        }
        double[][] tab = { { 1.0, 0.0, 0.0 }, { 0.0, Math.cos(Math.PI * A), -Math.sin(Math.PI * A) },
                { 0.0, Math.sin(Math.PI * A), Math.cos(Math.PI * A) } };
        Matrice reflexion = new Matrice(tab);
        this.aMatrice = reflexion.aMatrice;
    }

    public void setRotationHomogene3DX(double A) {
        if (aLine != 4 || aCol != 4) {
            System.out.println("Impossible, la matrice n'est pas carrée ou de bonne dimension");
            return;
        }
        double[][] tab = { { 1.0, 0.0, 0.0, 0.0 }, { 0.0, Math.cos(Math.PI * A), -Math.sin(Math.PI * A), 0 },
                { 0.0, Math.sin(Math.PI * A), Math.cos(Math.PI * A), 0 }, { 0.0, 0.0, 0.0, 1.0 } };
        Matrice reflexion = new Matrice(tab);
        this.aMatrice = reflexion.aMatrice;
    }

    public void rotation3DY(double A) {
        if (aCol != aLine || aCol != 3) {
            System.out.println("Impossible, la matrice n'est pas carrée ou de bonne dimension");
            return;
        }
        double[][] tab = { { Math.cos(Math.PI * A), 0, Math.sin(Math.PI * A) }, { 0.0, 1.0, 0.0 },
                { -Math.sin(Math.PI * A), 0.0, Math.cos(Math.PI * A) } };
        Matrice reflexion = new Matrice(tab);
        this.aMatrice = reflexion.aMatrice;
    }

    public void setRotationHomogene3DY(double A) {
        if (aLine != 4 || aCol != 4) {
            System.out.println("Impossible, la matrice n'est pas carrée ou de bonne dimension");
            return;
        }
        double[][] tab = { { Math.cos(Math.PI * A), 0, Math.sin(Math.PI * A), 0.0 }, { 0.0, 1.0, 0.0, 0.0 },
                { -Math.sin(Math.PI * A), 0.0, Math.cos(Math.PI * A), 0.0 }, { 0.0, 0.0, 0.0, 1.0 } };
        Matrice reflexion = new Matrice(tab);
        this.aMatrice = reflexion.aMatrice;
    }

    public void rotation3DZ(double A) {
        if (aCol != aLine || aCol != 3) {
            System.out.println("Impossible, la matrice n'est pas carrée ou de bonne dimension");
            return;
        }
        double[][] tab = { { Math.cos(Math.PI * A), -Math.sin(Math.PI * A), 0.0 },
                { Math.sin(Math.PI * A), Math.cos(Math.PI * A), 0.0 }, { 0.0, 0.0, 1.0 } };
        Matrice reflexion = new Matrice(tab);
        reflexion = this.produitMatriciel(reflexion);
        this.aMatrice = reflexion.aMatrice;
    }

    public void setRotationHomogene3DZ(double A) {
        if (aLine != 4 || aCol != 4) {
            System.out.println("Impossible, la matrice n'est pas carrée ou de bonne dimension");
            return;
        }
        double[][] tab = { { Math.cos(Math.PI * A), -Math.sin(Math.PI * A), 0.0, 0.0 },
                { Math.sin(Math.PI * A), Math.cos(Math.PI * A), 0.0, 0.0 }, { 0.0, 0.0, 1.0, 0.0 },
                { 0.0, 0.0, 0.0, 1.0 } };
        Matrice reflexion = new Matrice(tab);
        this.aMatrice = reflexion.aMatrice;
    }

    public double getCoefficient(final int pY, final int pX) {
        if (pX >= 0 && pX < this.aCol && pY >= 0 && pY < this.aLine) {

            return this.aMatrice[pX][pY];
        }
        return 0.0;
    }

    public boolean setCoefficient(final int pX, final int pY, final double pValue) {
        if (pX >= 0 && pX < this.aCol && pY >= 0 && pY < this.aLine) {
            this.aMatrice[pX][pY] = pValue;
            return true;
        }
        return false;
    }

    public Matrice getLine(int pIndex) {
        return new Matrice(this.aMatrice[pIndex]);
    }

    public int getNbLine() {
        return this.aLine;
    }

    public int getNbCol() {
        return this.aCol;
    }

    public void display() {
        for (double[] ds : aMatrice) {
            for (double d : ds) {
                System.out.print(d + " ");
            }
            System.out.println("");
        }
        System.out.println("");
    }

    public void setHomotethieHomogene3D(double k) {
        if (this.getNbCol() != 4 || this.getNbLine() != 4) {
            System.out.println("Pas les bonnes dimensions");
        }
        this.setIdentite();
        for (int i = 0; i < 3; i++) {
            this.aMatrice[i][i] = k;
        }
    }

    public void setTranslationHomogene3D(Vecteur v) {
        if (this.getNbCol() != 4 || this.getNbLine() != 4) {
            System.out.println("Pas les bonnes dimensions");
        }
        this.setIdentite();
        for (int i = 0; i < 3; i++) {
            this.aMatrice[i][i] = v.getCoordonnees(i);
        }
    }

    public void setProjectionOrthoOxyHomogene3d() {
        double[][] tab = { { 1.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 }, { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 } };
        this.aCol = 4;
        this.aLine = 3;
        this.aMatrice = tab;
    }

    public void setProjectionOrthoOxzHomogene3d() {
        double[][] tab = { { 1.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 1.0 }, { 0.0, 0.0, 0.0 } };
        this.aCol = 4;
        this.aLine = 3;
        this.aMatrice = tab;
    }

    public void setProjectionOrthoOyzHomogene3d() {
        double[][] tab = { { 0.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 }, { 0.0, 0.0, 1.0 }, { 0.0, 0.0, 0.0 } };
        this.aCol = 4;
        this.aLine = 3;
        this.aMatrice = tab;
    }

    public void setProjectionPerspectiveOxyHomogene3d(double d) {
        double[][] tab = { { 1.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 }, { 0.0, 0.0, 1 / d }, { 0.0, 0.0, 0.0 } };
        this.aCol = 4;
        this.aLine = 3;
        this.aMatrice = tab;
    }

}

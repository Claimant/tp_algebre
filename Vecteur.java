public class Vecteur {

    private double[] aCoord;

    public Vecteur(final int pN) {
        this.aCoord = new double[pN];
    }

    public Vecteur(final double[] pTab) {
        this.aCoord = pTab;
    }

    public void multScalaire(final double pS) {
        for (int i = 0; i < aCoord.length; i++) {
            aCoord[i] *= pS;
        }
    }

    public double norme() {
        double vAdd = 0;
        for (double d : aCoord) {
            vAdd += d * d;
        }
        return Math.sqrt(vAdd);
    }

    public void normalise() {
        if (this.norme() != 0.0) {
            this.multScalaire(1 / this.norme());
        } else {
            System.out.println("Division par zero");
        }
    }

    public void sommeVectorielle(final Vecteur pV) {
        if (this.getDimension() == pV.getDimension()) {

            for (int i = 0; i < aCoord.length; i++) {
                this.aCoord[i] += pV.getCoordonnees(i);
            }
        } else
            System.out.println("Pas la même dimension");
    }

    public double produitScalaire(final Vecteur pV) {
        if (this.getDimension() == pV.getDimension()) {
            double vSum = 0;
            for (int i = 0; i < aCoord.length; i++) {
                vSum += (this.aCoord[i] * pV.getCoordonnees(i));
            }
            return vSum;
        }
        System.out.println("Pas la même dimension");
        return -1;
    }

    public Vecteur produitVect3D(final Vecteur pV) {
        if (this.getDimension() == 3 && pV.getDimension() == 3) {
            double vX = this.getCoordonnees(1) * pV.getCoordonnees(2) - this.getCoordonnees(2) * pV.getCoordonnees(1);
            double vY = this.getCoordonnees(2) * pV.getCoordonnees(0) - this.getCoordonnees(0) * pV.getCoordonnees(2);
            double vZ = this.getCoordonnees(0) * pV.getCoordonnees(1) - this.getCoordonnees(1) * pV.getCoordonnees(0);
            double tab[] = { vX, vY, vZ };
            return new Vecteur(tab);
        }
        return null;
    }

    public boolean estOrthogonal(final Vecteur pV) {
        return (approche(this.produitScalaire(pV), 0.0, 0.0000001));
    }

    public boolean estColineaire(final Vecteur pV) {
        if (pV.getDimension() >= 1 && this.getDimension() >= 1) {
            double vRap = this.getCoordonnees(0) / pV.getCoordonnees(0);
            // System.out.println(vRap);
            for (int i = 1; i < aCoord.length; i++) {
                double vVal = this.getCoordonnees(i) / pV.getCoordonnees(i);
                // System.out.println(vVal);
                if (!this.approche(vVal, vRap, 0.0000001)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean estCoplanaire3D(final Vecteur pV1, final Vecteur pV2) {
        Vecteur pV3 = pV1.produitVect3D(pV2);
        if (pV3 != null) {
            return this.estOrthogonal(pV3);
        }
        return false;
    }

    public Vecteur getOrthogonalVecteur() {
        double[] tab = new double[this.getDimension()];
        tab[0] = aCoord[0] + 1;
        for (int i = 1; i < this.aCoord.length; i++) {
            tab[i] = aCoord[i];
        }
        Vecteur v2 = new Vecteur(tab);
        return this.produitVect3D(v2);
    }

    public double[] getTab() {
        return this.aCoord;
    }

    public double getCoordonnees(final int pI) {
        return this.aCoord[pI];
    }

    public int getDimension() {
        return this.aCoord.length;
    }

    public void setCoordonnee(final int pI, final double pVal) {
        this.aCoord[pI] = pVal;
    }

    public double getX() {
        if (this.aCoord.length >= 1) {
            return this.aCoord[0];
        }
        return -1.0;
    }

    public double getY() {
        if (this.aCoord.length >= 2) {
            return this.aCoord[1];
        }
        return -1.0;
    }

    public void displayVecteurInPlan(final Plan pPlan) {
        pPlan.dessinerVecteurEn2d(new VecteurEn2D(this.getX(), this.getY()));
    }

    public boolean approche(final double vD1, final double vD2, final double eps) {
        return (Math.abs(vD1 - vD2) <= eps);
    }

    public void display() {
        for (double d : aCoord) {
            System.out.print(" " + d);
        }
        System.out.println("");
    }

    public void normaliseHomogene() {
        double toDivideBy = this.getCoordonnees(this.getDimension() - 1);
        for (int i = 0; i < aCoord.length; i++) {
            this.aCoord[i] /= toDivideBy;
        }
    }

}

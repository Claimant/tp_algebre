public class VecteurEn2D {

    private double aX;
    private double aY;

    VecteurEn2D(final double pX, final double pY) {
        this.aX = pX;
        this.aY = pY;
    }

    VecteurEn2D(VecteurEn2D pV) {
        this.aX = pV.aX;
        this.aY = pV.aY;
    }

    public double norme() {
        return Math.sqrt(this.aX * this.aX + this.aY * this.aY);
    }

    public void multiplicationScalaire(final double pS) {
        this.aX *= pS;
        this.aY *= pS;
    }

    public void sommeVectorielle(final VecteurEn2D pV) {
        this.aX += pV.aX;
        this.aY += pV.aY;
    }

    public double produitScalaire(final VecteurEn2D pV) {
        return (this.aX * pV.aX + this.aY * pV.aY);
    }

    public boolean estOrthogonal(final VecteurEn2D pV) {
        return (this.produitScalaire(pV) == 0);
    }

    public VecteurEn2D obtenirVectOthogonal() {
        return new VecteurEn2D(this.aY, -this.aX);
    }

    public boolean estColineaire(final VecteurEn2D pV) {
        return (aX / pV.aX == aY / pV.aY);
    }

    public VecteurEn2D inverseVecteur() {
        return (new VecteurEn2D(-this.aX, -this.aY));
    }

    public VecteurEn2D createNewVecteur(final VecteurEn2D pV) {
        return (new VecteurEn2D(pV.aX - this.aX, pV.aY - this.aY));
    }

    public double getX() {
        return this.aX;
    }

    public double getY() {
        return this.aY;
    }

    public void displayVecteur() {
        System.out.println("Vect : " + this.aX + " / " + this.aY);
    }

    public boolean approche(final double vD1, final double vD2, final double eps) {
        return (Math.abs(vD1 - vD2) <= eps);
    }
}
